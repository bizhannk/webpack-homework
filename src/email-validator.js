const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com'];

export const validate = (email) => {
  let isValid = false;
  for (let ending of VALID_EMAIL_ENDINGS) {
    if (email.endsWith(ending)) {
      isValid = true;
    }
  }
  return isValid;
};
