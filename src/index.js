// style
import './styles/style.css';
// code
import { loadSection, SectionCreator } from './join-us-section.js';

const section = new SectionCreator();
//choose advanced or standart programm
section.createType('standart');

window.addEventListener('load', loadSection);
